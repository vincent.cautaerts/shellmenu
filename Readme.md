# Shell menu

## Installation & configuration

### Installation de `ShellMenu`

```.bash
mkdir -p ~/prg/bash/ShellMenu
cd ~/prg/bash/ShellMenu
git clone https://gitlab.com/vincent.cautaerts/shellmenu .
echo export SHELLMENU_HOME='~/prg/bash/ShellMenu' >> ~/.bashrc
```

### Création d'un menu pour un projet

- Choisir un nom, par exemple **Abc**
    - truc: commencer par majuscule pour avoir une auto-completion facile: 
       il suffira de taper 'A'+TAB pour avoir 'Abc' qui s'affiche (si c'est le
       seul programme qui commence par 'A'...)
- Copier la base de script 'Example' quelque part dans le projet sous le nom choisi
  - recommandation: `~/MonProjet/bin/Abc'
- s'assurer que le script est trouvé par l'OS:
    - soit l'ajouter le répertoire au `PATH` (via le `~/.bashrc`)
    ```bash
    export PATH=$PATH:~/MonProjet/bin/
    ```
    - soit ajouter un lien depuis un répertoire déjà dans le path
    ```bash
      cd ~/bin
      ln -s ~/MonProjet/bin/Abc .
    ```

Relancer votre shell (login/logout,...), et testez
```bash
cd / # ou n'importe où ailleurs...
Abc help
```

## Principes de base du script
Reconstruisons l'essentiel du script, étape par étape:

### Version ultra-simple

```bash
WHAT="$1"
shift
case "$WHAT" in
qqchose)
	: # fait quelque chose
	;;
esac
```

### Utilisation d'un `${ABC_HOME}`

- pointe vers la base du *projet*

```~/.bashrc
	export ABC_HOME=~/MonProjet
```

- permet d'avoir des commandes qui fonctionnent de la même manière d'où qu'on l'appelle.
- permet aux commandes de trouver leurs fichiers,...
- permet aux collègues d'utiliser le script `Abc` même si le projet est ailleurs chez eux

La commande de base devient:

```bash
WHAT="$1"
shift
case "$WHAT" in
qqchose)
    cd "${ABC_HOME}"
	: # fait quelque chose
	;;
esac
```

Note: je déconseille de faire un `cd "${ABC_HOME}"` en début de script, certaines
commandes peuvent dépendre du répertoire courant...

### Plusieurs commandes à la suite

Pour pouvoir exécuter plusieurs commandes à la suite

```bash
Abc command_1 command_2 command_3
```

ce qui est pratique:

- si les commandes durent, on les enchaîne
    - si une commande 'rate' (statut de retour != 0), la chaîne s'arrête
- on peut reprendre toute une chaîne de son historique

La commande de base devient:

```bash
while [ "$#" != 0 ] ; do
  WHAT="$1"
  shift
case "$WHAT" in
qqchose)
    cd "${ABC_HOME}"
	: # fait quelque chose
	;;
esac
done
```

### Bash fast fail

Voir aussi [bashstyle](https://gist.github.com/outro56/4a2403ae8fefdeb832a5) et `man bash`.

```bash
set -eo pipefail
```

Avec ceci:

- **erreur fatale** dès qu'un process retourne un statut !=0
    - pas dans un `if`,...
- **erreur fatale** s'il y a une erreur dans un 'pipe'

```bash
set -e -o pipefail
echo Hello | grep x | cut -c2-
```

Pour supprimer une erreur qui n'en est pas vraiment une

```bash
  command || true
```

### Function `die`

Cette petite fonction

```bash
function die() { echo "$(date --iso-8601=seconds) : $1" >&2; exit 1; }
```

ou cette version améliorée

```bash
function die() { echo "$(date --iso-8601=seconds) : $(tput setaf 1)$(tput setab 3) $1 $(tput sgr 0)" >&2; exit 1; }
```

est très pratique, pour terminer le script *et* afficher un message (sur la sortie
d'erreur, comme il se doit):

```bash
if ! [ -f "/etc/passwd" ]; then
  echo "Pas de fichier /etc/passwd !" >&2
  exit 1
```

peut devenir

```bash
[ -f "/etc/passwd" ] || die "Pas de fichier /etc/passwd !"
```

## log

Une autre fonction utile:

```bash
function log() {
        TPUT=tput
        [ -z "$TERM" -o "$TERM" == "dumb" ] && TPUT=:
        PREFIX=""
        INFIX=" "
        DATE="$(date +%Y-%m-%dT%H:%M:%S) : "
        IN="$($TPUT setaf 4)       "
        while true; do
          case "$1" in
          --prefix=*)
                  PREFIX="${1##--prefix=}"
                  ;;
          --infix=*)
                  INFIX="${1##--infix=} : "
                  ;;
          --nodate)
                  DATE=""
                  ;;
          --warn)
                  IN="$($TPUT setaf 3)WARNING"
                  ;;
          --error)
                  IN="$($TPUT setaf 1)ERROR  "
                  ;;
          --note)
                  IN="$($TPUT setaf 2)NOTE   "
                  ;;
          *)
                  break
                  ;;
          esac
          shift
        done
        echo "${PREFIX}${DATE}${INFIX}$IN :" $@ "$($TPUT sgr0)"

}
```

### Commandes plus souples

**Attention** à force de rendre ces commandes plus souples, on
finit par avoir trop d'options, et plus d'intérêt à ne pas appeler
les commandes complètes...


#### Modules, raccourcis,...
Exemple d'utilisation

```bash
Abc start:mod2
Abc start:vl   # alias pour 'SomeVeryLongModuleName'
```

On ne veut écrire qu'une commande 'start'...

```bash
function to_mod {
  case "$1" in
mod1|mod2|SomeVeryLongModuleName) echo "$1" ;;
vl) echo "SomeVeryLongModuleName" ;;
*)
  die "Can't find module '$1'";;
  esac
}

case "$WHAT" in
start:*)
  cd "${ABC_HOME}"
  mod="$( to_mod "$(echo "${WHAT}" | cut -d: -f2)")"
  docker-compose up -d "${mod}"
   ;;
esac
```

### Commandes `pre-`, `post-` et `rep-'

Voir les commentaires dans `menuHelper.sh`

Permet d'adapter les commandes à des particularités de l'environnement
d'un développeur:
- lancer une commande spécifique avant la commande normale (`pre-`)
- lancer une commande spécifique après la commande normale (`post-`)
- lancer une commande spécifique à la place d'une commande normale (`rep-`)

### Prefixer la sortie d'une commande
Pour prefixer chaque ligne sortie par une commande:
```bash
ls | sed 's/^/|ls>   /';
```

### Aide
Si les commandes deviennent nombreuses, une petite aide peut être utile...
La commande suivante imprime un petit texte, puis cherche tous les commentaires
d'aide dans les `case...esac` du script...

```bash
SELF="${BASH_SOURCE[0]}"
case "${WHAT}" in
help)   #!Help: display help about documented commands
  	echo "Sample script for shortcuts and common actions concerning project Abc"
	echo
	cat "${SELF}" | grep -E '^\s*(.*)\)\s*#!Help: (.*)'
  ;;
```

## Version finale avec 'menuHelper.sh'

Pour ne pas avoir toute la logique copiée dans chaque script, les
parties génériques ont été extraites dans le script d'aide `menuHelper.sh`.

Le script d'exemple `Abc` montre comment utiliser `menuHelper.sh`.

## Conseils finaux

- partez de l'exemple, et customizez-le !
- copiez 'Abc' pour en faire votre script, dans votre projet, mais gardez `menuHelper.sh` intact pour profiter de révisions futures. 
- essayez que le statut de chaque commande soit représentatif:
  - 0 : tout va bien
  - != 0 : il faut interrompre le traitement
  - pour pouvoir enchaîner en toute sécurité les commandes
- si vous utilisez ce script dans votre projet
    - documentez son existence
    - améliorez le peu à peu
      - utilisez les variables, le 'pre-', 'post-' ou 'rep-' pour l'adapter à votre
          config personnelle (<> de celles de collègues sur le même projet)
- un script comme 'Abc' se construit petit à petit, avec:
  - les commandes utiles qu'on reprend souvent
  - les commandes compliquées qu'on ne veut pas perdre
  - les commandes persos iront plutôt dans `private/bin/`

## Pour aller plus loin
- `man bash`
- [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/index.html)
- [Advanced Bash-Scripting Guide](https://tldp.org/LDP/abs/html/index.html)
