#!/bin/bash

# Demo de l'effet de 'pipefail'
# Une command *dans* un 'pipe' (pas la dernière) rate.
# Ignoré avec '+o pipefail', prise en compte (par le '-e') avec '-o pipefail'

echo "<Sans pipefail...>"
set -e +o pipefail
echo Hello | grep x | cut -c2-
echo "<Sans>"

echo "<Avec pipefail...>"
set -e -o pipefail
echo Hello | grep x | cut -c2-
echo "</Avec>"


