#!Source_This_Dont_execute_it


# Read a properties file, and define variables in the environment for the properties
# An optional prefix can be given, and will be added in front of key names to
#  make environment variable names.
function read_properties() {
  FILE="$1"
  PREFIX="${2:-x}"
  while IFS='=' read -r key value; do
    if [[ "$key" =~ ^\s*\# || "$key" = "" ]]; then
      continue
    fi
    export "${PREFIX}$key=$value"
  done < <( cat "$FILE"; echo )
}
export -f read_properties


function check_url() {
  local expected_content=""
  local temp_content=""
  local temp_target="/dev/null"
  local cookiep=()
  while true; do
    if [ "$1##--" == "$1" ]; then
      break
    fi
    case "$1" in
      --content=*)
          expected_content="${1##--content=}"
          temp_content="$(mktemp)"
          temp_target="${temp_content}"
        ;;
      --cookie=*)
        cookiep=("--cookie" "${1##--cookie=}")
        ;;
      --)
        shift
        break
        ;;
      --*)
        die "Unexpected option for check_url: '$1'"
        ;;
      *)
        # no shift !
        break
    esac
    shift
  done
  declare subject="$1" url="$2"
    # check that the URL answers with a '200' status code.
    # Note that the URL might be eg 'http://varnish!kiosque:1234/test' which means that we'll contact
    #  the server 'varnish', but with a 'Host' header 'kiosque'

    # urls of the form http://real_host.be!fake_host/liseuse/ping.php
    #  which means 'contact real_host.be with a host header fake_host'
    if [[ "$url" =~ ^(https?://[^:/]+)!([^:/]+)((:\d+)?(/.*)?)$ ]]; then
      url="${BASH_REMATCH[1]}${BASH_REMATCH[3]}"
      add_host="--header 'Host:${BASH_REMATCH[2]}'"
    else
      # same principle, but easier to write with replacements
      #   (fake_host)http://real_host.be/liseuse/ping.php
      if [[ "$url" =~ ^\((.*)\)(https?://.*)$ ]]; then
        url="${BASH_REMATCH[2]}"
        add_host="--header 'Host:${BASH_REMATCH[1]}'"
      else
        add_host=''
      fi
    fi
    # echo "# URL='$url' add_host='$add_host'" "${cookiep[@]}"
    echo -n "$(printf %-30s "$subject") : "
    local status="$( curl --insecure --max-time 5 "${cookiep[@]}" --header "Cache-Control: no-cache" ${add_host} --write-out "%{http_code}\n" --silent --output ${temp_target} "$url" 2>&1 || true )"
    if [ "$status" == "200" ]; then
      if [[ ! -z "${temp_content}" ]]; then
        local rec_content="$( cat ${temp_target} )"
        if [[ "$rec_content" != "$expected_content" ]]; then
          log --error --nodate 'Reply KO (bad content)'
        else
          log --note --nodate 'Reply OK (with content)'
        fi
      else
        log --note --nodate 'Reply OK'
      fi
    else
      log --error --nodate "Reply KO : '$status'"
    fi
    if [[ -e "${temp_content}" ]]; then
      rm "${temp_content}"
    fi
}
export -f check_url

function onError() {
	test -t 1 && tput setf 4
	echo "Abnormal termination: line ${BASH_LINENO[0]} of '${FUNCNAME[1]}' in file ${BASH_SOURCE[1]}" >&2
	caller >&2
	test -t 1 && tput sgr0 # Reset terminal
	if [ ! -z "${DAM_OPTS['pause']}" ]; then
		read -p "ERROR! Press any key to continue" -n1 -s
		echo
	fi
}
function setOnError() {
	set -e
	set -o pipefail
	trap "onError" ERR
}
function checkExist() {
	M="not found"
	if [ "$1" == "-x" ]; then
		OP="-x"
		OP_D="-x"
		M="not found or not executable"
		shift
	elif [ "$1" == "-r" ]; then
		OP="-r"
		OP_D="-x"
		M="not found or not readable"
		shift
	else
		OP="-f"
		OP_D="-d"
	fi
	for p in "$@"; do
		if [ "${p%%/}" != "$p" ]; then # path
			[ "$OP_D" "$p" ] || die "Folder '$p' not found... aborting"
		else
			[ "$OP" "$p" ] || die "File '$p' $M... aborting"
		fi
	done
}

function stdintoexitstatus() {
	read exitstatus
	return $exitstatus
}
function runPrefix() {
	# Usage: runPrefix PREF_OUT PREF_ERR command arg1 arg2 ...
	#   see http://unix.stackexchange.com/questions/14270/get-exit-status-of-process-thats-piped-to-another/73180#73180
	# Run the given commands, filtering its STDIN and STDERR, and return the result of the command.
	local pout
	local perr
	local T
	local F
	local status
	pout=$1
	perr=$2
	shift
	shift
	{
		{
			{
				{
					{
						{
							( eval "$@" );
							status="$?"
							echo $status >&$T;
						} | sed -e "s/^/$pout/";
					} 2>&1 1>&$F  ;
				}  | sed -e "s/^/$perr/";
			} {T}>&1 1>&2 2>&- ;
		} | stdintoexitstatus
} {F}>&1
	out=$?
	exec {F}>&-
	return $out
}

# Takes a lock before running an action
# If the lock can't be taken, abort immediately while returning 75 (EX_TEMPFAIL)
# If a given process must take different locks at the same time, each should specify a
# different FD to use (defaults to 200) by adding eg '-199' as first argument for FD 199.
# Note: this is a OS lock: it will be realease even if the process is killed,...
# The lock file will not be deleted, and it's presence doesn't mean that the lock is taken.
function runLock() { # usage: runLock 'LockName prg arg1 arg2'   OU 'runLock -200 LockName prg arg1 arg2...'
	local FD=200
	if [ "${1##-}" != "$1" ]; then
		FD="${1##-}"
		shift
	fi
	local prefix="$1"
	shift

	local lock_file
	if [ "${prefix##/}" != "${prefix}" ]; then
		lock_file="${prefix}"
	else
		local lock_dir="${LOCK_DIR:-/tmp}"
		lock_file="$lock_dir/${prefix}.lock"
	fi
	(
		eval "exec $FD>$lock_file"
		flock -n "$FD" || return 75
		#echo "# $$: Running $@" >&2
		eval "$@"
		#echo "# $$: Finished running $@" >&2
	)
}
