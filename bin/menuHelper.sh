#!Source_This_Dont_execute_it
# 'source' this file from your menu definition, ie: do ". path/to/this/file/menuHelper.sh"

# This is only for bash ! Sorry !
if [ -z "${BASH_VERSION-}" ]; then
    return 1;
fi

set -e -o pipefail

if ! command -v tput &> /dev/null || [ -z "$TERM" ] || [ "$TERM" = "dumb" ]; then
    # Define a dummy tput function that does nothing
    tput() { :; }
fi

# Easily exit with a message (on stderr) and a status code
function die() {
  local EXIT_CODE="1"
  while [ "$#" != "0" ]; do
    case "$1" in
      -[0-9]*) EXIT_CODE="${1##-}"; shift; ;;
      *) break;;
    esac
  done
  echo "$(date --iso-8601=seconds) : $(tput setaf 1)$(tput setab 3)" "$@" "$(tput sgr 0)" >&2;
  exit "${EXIT_CODE}";
}
export -f die

# BASH_SOURCE[0] points to this file
# BASH_SOURCE[1] points to the file that called this file
if [[ -z "${PROJECT_SELF_SCRIPT:-}" ]]; then
  declare -r PROJECT_SELF_SCRIPT="$( realpath "${BASH_SOURCE[1]}" )"
fi
if [[ -z "${PROJECT_ID:-}" ]]; then
  # PROJECT_ID not specified => extract it from the name of the script calling this function
  declare -r PROJECT_ID="$( basename "${PROJECT_SELF_SCRIPT}" )"
fi
declare -r PROJECT_ID_UPPER="$( echo "${PROJECT_ID}" | tr a-z A-Z)"
declare -r PROJECT_ID_LOWER="$( echo "${PROJECT_ID}" | tr A-Z a-z)"

# If the project ID is 'ABC', we define (if it doesn't exists yet) a 'ABC_HOME' variable
# If this autodetection doesn't work, define it in your '~/.bashrc' (for example)
if [[ -z "$( eval "echo \$${PROJECT_ID_UPPER}_HOME" )" ]]; then
  # set a variable like 'ABC_HOME' with the project home dir (syn. to PROJECT_HOME)
  # Suppose that the project home is the folder in which the script is...
  __TMP_HOME="$( dirname "${PROJECT_SELF_SCRIPT}" )"
  # ... or its parent if it ends in '.../bin'
  __TMP_HOME="${__TMP_HOME%%/bin}"
  declare -r "${PROJECT_ID_UPPER}_HOME"="${__TMP_HOME}"
  unset __TMP_HOME
fi

# For generic scripts, we also define a 'PROJECT_HOME'
if [[ -z "${PROJECT_HOME}" ]]; then
  PROJECT_HOME="$( eval "echo \$${PROJECT_ID_UPPER}_HOME" )"
fi

[ -z "${PROJECT_ID}" ] && die "Variable 'PROJECT_ID' not properly setup (empty)!"
[ "$( echo "${PROJECT_ID}" | tr -d a-zA-Z0-9_.- )" == "" ] || die "Variable 'PROJECT_ID' not properly setup (bad chars)!"
[ -d "${PROJECT_HOME}" ] || die "Variable 'PROJECT_HOME' not properly setup!"

# Analyses the arguments given when calling this 'menuHelper.sh'
#  *not the arguments given to the main command ! (see 'parse_args' for that)
declare TMP_MENU_SECRETS="${PROJECT_HOME}/private/secrets.txt"
while (( "$#" )); do
  WHAT="$1"
  shift
  case "$WHAT" in
    '')
      : # when sourcing without argument, the sourced file receives the args from the main '.sh'
      # so we ask the user to always give at least on '' arg, and we ignore it here.
      ;;
    --secrets=*)
      TMP_MENU_SECRETS="$( realpath "${PROJECT_HOME}/${WHAT##--secrets=}" )"
      [ -e "$TMP_MENU_SECRETS" ] || die -253 "Can't find secret file '$TMP_MENU_SECRETS'"
      ;;
    --*)
      die "Unknown option '$WHAT' when sourcing '$0'"
      ;;
    *)
      die "Unknown command '$WHAT' when sourcing '$0'"
  esac
done
declare -r MENU_SECRETS="${TMP_MENU_SECRETS}"
unset TMP_MENU_SECRETS

# to be overwritten by user. It can be used eg to setup environment
#  based on the option read so far
function pre_cmd() {
  :
}

declare -A MENU_OPTS
function parse_args() {
  if [[ "$#" == "0" ]]; then
    exec "$PROJECT_SELF_SCRIPT" select
  fi
  while (( "$#" )); do
    case "$1" in
    --*=*)
      local KEY="$( echo "${1##--}" | cut -d= -f1 )"
      local VAL="$(echo "$1" | cut -d= -f2-)"
      if [ -z "$VAL" ]; then # to unset an option, just specify '--option='
        unset MENU_OPTS["$KEY"]
      else
        MENU_OPTS["$KEY"]="$VAL"
      fi
      shift
      continue
      ;;
    --*)
      MENU_OPTS+=(["${1#--}"]="1")
      shift
      continue
      ;;
    *)
      local MENU_ARG="$1"
      shift
      local MENU_ARGS=()
      local MENU_CMD
      # a command like 'action:a:b:c' gives 1 command 'action' with 3 arguments 'a', 'b', 'c'
      case "$MENU_ARG" in
      *:*)
        MENU_CMD="$(echo "$MENU_ARG" | cut -d: -f1)"
        IFS=":" read -ra MENU_ARGS <<<"$(echo "$MENU_ARG" | cut -d: -f2-)"
        ;;
      *)
        MENU_CMD="$MENU_ARG"
        ;;
      esac
      # another way to give arguments: '{' a b c '}'
      # If the arguments include ':', this is the only proper way...
      if [ "${1:-}" == "{" ]; then
        shift
        while [ "$#" != "0" ]; do
          if [ "$1" == "}" ]; then
            shift
            break
          fi
          MENU_ARGS+=("$1")
          shift
        done
      fi

      local DONE=""
      # OK, now options and arguments are done

      pre_cmd

      # We want to be able to 'customize' actions, by running (sourcing)
      #  programs before a command, in place of a command, or after a command.
      # This will be done depending on the presence of scripts with appropriate names.
      CMD="$( which "${PROJECT_ID}-pre-${MENU_CMD}"  2>&1 || true )"
      if [[ -n "${CMD}" && -x "${CMD}" ]]; then
        "${CMD}" "${MENU_CMD}" "${MENU_ARGS[@]}"
      elif [ -f "${PROJECT_HOME}/private/bin/${PROJECT_ID}-pre-${MENU_CMD}.sh" ]; then
        # executed before actions below
        . "${PROJECT_HOME}/private/bin/${PROJECT_ID}-pre-${MENU_CMD}.sh" "$MENU_CMD" "${MENU_ARGS[@]}"
      fi
      CMD="$( which "${PROJECT_ID}-rep-${MENU_CMD}"  2>&1 || true )"
      if [[ -n "${CMD}" && -x "${CMD}" ]]; then
        "${CMD}" "${MENU_CMD}" "${MENU_ARGS[@]}"
        DONE="1"
      elif [ -f "${PROJECT_HOME}/private/bin/${PROJECT_ID}-rep-${MENU_CMD}.sh" ]; then
        # replaces below instructions
        . "${PROJECT_HOME}/private/bin/${PROJECT_ID}-rep-${MENU_CMD}.sh" "$MENU_CMD" "${MENU_ARGS[@]}"
        DONE="1"
      else
        run_one_cmd "$MENU_CMD" "${MENU_ARGS[@]}"
      fi
      CMD="$( which "${PROJECT_ID}-post-${MENU_CMD}"  2>&1 || true )"
      if [[ -n "${CMD}" && -x "${CMD}" ]]; then
        "${CMD}" "${MENU_CMD}" "${MENU_ARGS[@]}"
      elif [ -f "${PROJECT_HOME}/private/bin/${PROJECT_ID}-post-${MENU_CMD}.sh" ]; then
        # executed before actions below
        . "${PROJECT_HOME}/private/bin/${PROJECT_ID}-post-${MENU_CMD}.sh" "$MENU_CMD" "${MENU_ARGS[@]}"
      fi
      ;;
    esac
  done
}

# Utility function to retrieve information, when needed, from the secret file.
#  By default, the secret file is in "$PROJECT_HOME/private/secrets.txt", but this
#  can be changed with the argument '--secrets=' when sourcing 'menuHelper.sh'
function get_secret {
  declare KEY="$1"
  cat "${MENU_SECRETS}" | grep "^${KEY}=" | head -1 | cut -d= -f2-
}
export -f get_secret

function project_info { # may be overriden by user
  :
}

function run_build_in() {
  local CMD="$1"
  shift
  FCMD="$( which "${PROJECT_ID}-${CMD}"  2>&1 || true )"
  if [[ -n "${FCMD}" && -x "${FCMD}" ]]; then
    # command, execute it
    exec "${FCMD}" "$@"
  else
    FCMD="$( which "${PROJECT_ID}-${MENU_CMD}.sh"  2>&1 || true )"
    if [[ -n "${FCMD}" && -x "${FCMD}" ]]; then
      # shell, source it
      source "${FCMD}" "$@"
      exit $?
    fi
  fi
  case "$CMD" in
    bash)
      exec bash
      ;;
    help)   #!Help: Display help for this script
      local HI="$(tput setaf 3)"
      local NO="$(tput sgr 0)"
      if [ -n "$1" ]; then
        cmd="$1"
        fcmd="${PROJECT_HOME}/bin/do_${PROJECT_ID}_${cmd}.sh"
        if [ -f "${PROJECT_HOME}/bin/do_${PROJECT_ID}_${cmd}.sh" ]; then
          echo "+ HELP for $cmd command of project '$PROJECT_ID' (in ${PROJECT_HOME})"
          cat "${fcmd}" | sed -n '/:<<HELP/,/HELP/p' | sed '1d;$d'
        fi
      else
        echo "+- HELP for project '$PROJECT_ID' (in ${PROJECT_HOME})"
        ( project_help || echo "(function 'project_help' no defined)" ) | sed "s/^\(.*\)/|  $HI\1$NO/"
        echo "+- Custom commands:"
        local HI="$(tput setaf 4)"
        cat "${PROJECT_SELF_SCRIPT}" | sed -n 's/\s*\((\?\([^)]\+\))\)\?.*#\!Help\((\([^)]\+\))\)\?:\s*\(.*\)/\2§\4§\5/p' \
            | awk -v "HI=$HI" -v "NO=$NO" -F '§' '{if ($2!=""){$1=$2}; $1 = sprintf("|   %s%-30s%s", HI, $1, NO); $2=""} {print}'
        for fcmd in "${PROJECT_HOME}/bin/do_${PROJECT_ID}_"*".sh"; do
          if [ ! -f "$fcmd" ]; then continue; fi
          cmd="$(basename "${fcmd%.sh}")"
          cmd="${cmd#do_${PROJECT_ID}_}"
          # echo "*** => add command '$cmd'"
          # cat "${fcmd}" | sed -n '/:<<HELP/,/HELP/p' | sed '1d;$d'
          cat "${fcmd}" | sed -n '/:<<HELP/,/HELP/p' | sed -n '2p' | sed "s|^|$cmd§§|" \
            | awk -v "HI=$HI" -v "NO=$NO" -F '§' '{if ($2!=""){$1=$2}; $1 = sprintf("|   %s%-30s%s", HI, $1, NO); $2=""} {print}'
        done

        echo "+- Common commands:"
        cat "${BASH_SOURCE[0]}" | sed -n 's/\s*\((\?\([^)]\+\))\)\?.*#\!Help\((\([^)]\+\))\)\?:\s*\(.*\)/\2§\4§\5/p' \
            | awk -v "HI=$HI" -v "NO=$NO" -F '§' '{if ($2!=""){$1=$2}; $1 = sprintf("|   %s%-30s%s", HI, $1, NO); $2=""} {print}'
      fi
      ;;
    bash-completions) #!Help: Generate bash completions
      # add this to eg ~/.bashrc:
      #   [ -d "$ABC_HOME" ] && eval "$( Abc bash-completions )"
      local WORDS=()
      # extract a list of words
      while read W; do
        WORDS+=("$W")
      done < <( cat "${PROJECT_SELF_SCRIPT}" "${BASH_SOURCE[0]}" \
        | sed -n 's/\s*\((\?\([^)]\+\))\)\?.*#\!Help\((\([^)]\+\))\)\?:\s*\(.*\)/\4 \2/p' \
        | awk '{print $1}' | sed 's/[|\n]/ /g' )
      # concat all of them into one variable, space separated
      local WORDS_IN_ONE="${WORDS[*]}"
      # output the completion script (escaping any '$' that needs to be in the output, but replacing
      # both $WORDS_IN_ONE and $PROJECT_ID (so that multiple projects don't interfere)
      cat <<-COMPLETION
          _complete_${PROJECT_ID}() {
                  local cur
                  _get_comp_words_by_ref -n : cur
                  local suggestions=(\$(compgen -W "${WORDS[*]}" -- "\$cur"))
                  COMPREPLY=("\${suggestions[@]}")
                  __ltrim_colon_completions "\$cur"
          }
          complete -F _complete_${PROJECT_ID} "${PROJECT_ID}"
COMPLETION
      ;;
    select) #!Help: Propose a selection of commands (without arguments...)
      local SEL=()
      local choice
      local HI="$(tput setaf 3)"
      local NO="$(tput sgr 0)"
      while read L; do
        SEL+=("$L")
      done < <( cat "${PROJECT_SELF_SCRIPT}" "${BASH_SOURCE[0]}" \
        | sed -n 's/\s*\((\?\([^)]\+\))\)\?.*#\!Help\((\([^)]\+\))\)\?:\s*\(.*\)/\2§\4§\5/p' \
        | awk -v "HI=$HI" -v "NO=$NO" -F '§' '{if ($2!=""){$1=$2}; $1=sprintf("%s %s%s%s%s%s",$3,HI,"[",$1,"]",NO);} {print $1}' )
      SEL+=("Exit this menu $HI[exit]$NO")
      select choice in "${SEL[@]}"; do
        # be carefull: there might be escape codes (due to '$HI' and '$NO' above) !
        if [[ "$choice" =~ ^.*\[([^|]*)(\|.*)?\][^]]* ]]; then
          choice="${BASH_REMATCH[1]}"
          if [[ "$choice" == "exit" ]]; then
            break;
          else
            "$PROJECT_SELF_SCRIPT" "$choice"
          fi
        else
          if [[ "$REPLY" == "q" ]]; then
            break;
          fi
          "$PROJECT_SELF_SCRIPT" "$REPLY"
        fi
      done
      ;;
    info) #!Help: Display some setup info
      echo "Env:"
      echo "  PROJECT_ID='${PROJECT_ID}'"
      echo "  PROJECT_HOME='${PROJECT_HOME}'"
      echo "  ${PROJECT_ID_UPPER}_HOME='"$( eval "echo \$${PROJECT_ID_UPPER}_HOME" )"'"
      echo "  MENU_SECRETS='${MENU_SECRETS}"
      echo "  PROJECT_SELF_SCRIPT='${PROJECT_SELF_SCRIPT}"
      project_info
      ;;
    exit) #!Help: Prematurely exits the script
      exit
      ;;
    sleep) #!Help(sleep): Sleep some time (eg: sleep:3600 or sleep:60m or sleep:1h)
      local T="$1"
      if [ -z "$T" ]; then T=1m; fi
      local S=0
      if [ "${T%%h}" != "$T" ]; then S=$(( ${T%%h}*3600 ));
      elif [ "${T%%m}" != "$T" ]; then S=$(( ${T%%m}*60 ));
      elif [ "${T%%s}" != "$T" ]; then S=$(( ${T%%s} ));
      else S=$(( 0+$T )); fi

      is_quiet || log "Sleeping $S seconds (T=$T)..."
      sleep $S
      is_quiet || log "Slept $T seconds..."
      ;;
    screen) #!Help: launch remaining commands into a screen
      #extract next commands from the list
      if [ "$1" == "{" ]; then
        shift
        ARGS=()
        while [ "$#" != "0" ]; do
          if [ "$1" == "}" ]; then
            shift
            break
          fi
          ARGS+=("$1")
          shift
        done
      else
        while [ "$#" != "0" ]; do
          ARGS+=("$1")
          shift
        done
      fi
      screen -l -d -m -s "Menu ${PROJECT_ID} for ${ARGS[0]}" "${PROJECT_SELF_SCRIPT}" "${ARGS[@]}"
      ;;
    tee|tee:*) #!Help : copy output for the next commands to specified file
      # note: the 'LOG_FILE' might be used in a following 'mail' command !
      export LOG_FILE="$( echo "${WHAT}" | cut -d: -f2- )"
      [ -z "$LOG_FILE" ] && LOG_FILE="/tmp/Abc-$$.log"
      exec > >( ts '%Y-%m-%dT%H:%M:%S' | tee "$LOG_FILE" ) 2>&1 ;
      ;;
    exec) #!Help: execute the command given in next params
      if [ "$1" == "{" ]; then
        shift
        ARGS=()
        while [ "$#" != "0" ]; do
          if [ "$1" == "}" ]; then
            shift
            break
          fi
          ARGS+=("$1")
          shift
        done
      else
        while [ "$#" != "0" ]; do
          ARGS+=("$1")
          shift
        done
      fi
      "${ARGS[@]}"
      ;;
    date) #!Help: Just outputs the current date and time
      date '+%Y-%m-%dT%H:%M:%S'
      ;;
    *)
      # check first if we can find program to run the command
      CMD_SH="${PROJECT_HOME}/bin/do_${PROJECT_ID}_${CMD}.sh"
      if [ -f "${CMD_SH}" ]; then
        . "${CMD_SH}" "$@"
      else
        die "Unknown command '$CMD'"
      fi
      ;;
  esac
}

# Utility function to call for messages that should be suppressed when '--quiet' option is set
#  eg:    is_quiet || echo "# Some informative message"
function is_quiet() {
  if [ ${MENU_OPTS[quiet]+_} ]; then return 0; else return 1; fi
}

# Consistent and easy logging
function log() {
  TPUT=tput
  [ -z "$TERM" -o "$TERM" == "dumb" ] && TPUT=:
  PREFIX=""
  INFIX=" "
  DATE="$(date +%Y-%m-%dT%H:%M:%S) : "
  IN="$($TPUT setaf 4)       "
  while true; do
    case "$1" in
    --prefix=*)
            PREFIX="${1##--prefix=}"
            ;;
    --infix=*)
            INFIX="${1##--infix=} : "
            ;;
    --nodate)
            DATE=""
            ;;
    --warn)
            IN="$($TPUT setaf 3)WARNING"
            ;;
    --error)
            IN="$($TPUT setaf 1)ERROR  "
            ;;
    --note)
            IN="$($TPUT setaf 2)NOTE   "
            ;;
    *)
            break
            ;;
    esac
    shift
  done
  echo "${PREFIX}${DATE}${INFIX}$IN :" "$@" "$($TPUT sgr0)"
}
export -f log

# Easily add one or more path(es) to $PATH, if its not already in it
function addPath {
	local var
	for var in "$@"; do
		if [ -z "$var" ]; then continue; fi
		if ! echo "$PATH" | grep -q "$var"; then
			PATH="${var}:$PATH"
		fi
	done
}
export -f addPath
